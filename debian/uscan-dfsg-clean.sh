#!/bin/sh 

# This script removes
# * cardsets whose copyright status is not DFSG-compatible or unclear
# * "standard" cardsets which are already included with the pysolfc package
# * some spurious .thumbnails and .xvpics directories
# * the t/ and png/ directories from cardset-konqi-modern
# * some duplicate files, and replaces them by symlinks
# For the latter task, the rdlinks and symlinks packages are required.
#
# Note that the file duplicates and spurious directories are removed in
# this step already instead in debian/rules as to reduce the size of
# the orig tarball. For replacement of duplicates by symlinks to work,
# we need to remove the "standard" cardsets beforehand.

set -e

#command --upstream-version version filename

[ $# -eq 3 ] || exit 255

echo

STANDARD_CARDSETS="2000 \
          crystal-mahjongg \
          dashavatara-ganjifa \
          dondorf \
          gnome-mahjongg-1 \
          hexadeck \
          kintengu \
          matrix \
          mughal-ganjifa \
          oxymoron \
          standard \
          tuxedo \
          vienna-2k"

NON_DFSG_CARDSETS="patience \
          rangoon-d \
          rangoon-e \
          rangoon-r \
          tksol"

CARDSETS_WITH_THUMBNAILS="fine-art-tarot \
          gdkcard-bonded \
          nicu-small \
          warwick \
          xpat2-nox-large"

CARDSETS_WITH_XVPICS="konqi-modern \
          warwick"

version="$2"
filename="$3"
dfsgversion=`dpkg-parsechangelog | sed -n '/^Version/{s/Version: \(.*\)+dfsg\([0-9]*\)-[0-9]*$/\2/p}'`
dfsgfilename=`echo $3 | sed "s,\.orig\.,+dfsg${dfsgversion}.orig.,"`

tar xfj ${filename}

dir=`tar tfj ${filename} | head -1 | sed 's,/.*,,g'`
dfsgdir=${dir}+dfsg${dfsgversion}
rm -f `readlink -f ${filename}`
rm -f ${filename}

# Remove non-dfsg cardsets
for c in ${NON_DFSG_CARDSETS}
do \
    rm -rf ${dir}/cardset-${c}; \
done
mv ${dir} ${dfsgdir}

# the standard set is already in the main package
for c in ${STANDARD_CARDSETS}; do \
    rm -rf ${dfsgdir}/cardset-${c}; \
done

# Remove .thumbnails/ directories
for c in ${CARDSETS_WITH_THUMBNAILS}
do \
    rm -rf ${dfsgdir}/cardset-${c}/.thumbnails; \
done

# Remove .xvpics/ directories
for c in ${CARDSETS_WITH_XVPICS}
do \
    rm -rf ${dfsgdir}/cardset-${c}/.xvpics; \
done

# Remove t/ and png/ directory from cardset-konqi-modern
rm -rf ${dfsgdir}/cardset-konqi-modern/t
rm -rf ${dfsgdir}/cardset-konqi-modern/png

# Replace duplicate files with symlinks
rdfind -outputname /dev/null -makesymlinks true ${dfsgdir}
# Fix those symlinks to make them relative
symlinks -r -s -c ${dfsgdir}

tar cf - ${dfsgdir} | bzip2 -9 > ${dfsgfilename}

rm -rf ${dfsgdir}

echo "${dfsgfilename} created."
